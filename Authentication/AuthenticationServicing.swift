//
//  AuthenticationServicing.swift
//  Authentication
//

import Foundation
import RxSwift
import Firebase

public typealias AuthorizedUser = User

public protocol AuthenticationObserving: class {
    var currentUser: AuthorizedUser? { get }

    func checkAuthStatus() -> Observable<AuthenticationStatus>
}

public protocol AuthenticationManaging: AuthenticationObserving {
    
    /// Sign out
    func signOut() -> Observable<AuthenticationStatus>
    
    func signIn(verificationCode: String) -> Observable<AuthenticationStatus>
    
    func link(email: String, password: String) -> Observable<AuthenticationStatus>
    
    /// Verify Phone Number
    func verify(phoneNumber: String) -> Observable<String>
    
    /// Verify code
    func verify(verificationCode: String)
    
    func update(displayName: String?, username: String?) -> Observable<AuthorizedUser>
}

public protocol AuthenticationServicing: AuthenticationManaging {

    /// Authentication status
    var authenticationStatus: Observable<AuthenticationStatus> { get }
}

public enum AuthenticationStatus {
    case loggedIn(AuthorizedUser?)
    case loggedOut
    case error(Error)
}

extension AuthorizedUser {
    public var isNew: Bool {
        return !hasEmail || !hasDisplayName
    }
    
    public var hasEmail: Bool {
        return !(email?.isEmpty ?? true)
    }

    public var hasDisplayName: Bool {
        return !(displayName?.isEmpty ?? true)
    }
}

public final class AuthenticationService: NSObject {
    
    func createService(source: AuthenticationServiceSource) -> AuthenticationServicing? {
        return nil
    }
}


public enum AuthenticationServiceSource {
    case firebase
}
