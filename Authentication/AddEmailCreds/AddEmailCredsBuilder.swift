//
//  AddEmailCredsBuilder.swift
//  Authentication
//

import RIBs
import UserInterface

protocol AddEmailCredsDependency: Dependency {
    var themeStream: ThemeStreaming { get }
    var backendService: AuthenticationServicing { get }
    var keyboardEventObserver: KeyboardObserving { get }
}

final class AddEmailCredsComponent: Component<AddEmailCredsDependency> { }

// MARK: - Builder

protocol AddEmailCredsBuildable: Buildable {
    func build(withListener listener: AddEmailCredsListener?, mode: EditingMode) -> AddEmailCredsRouting
    func build(withListener listener: AddEmailCredsListener?) -> AddEmailCredsRouting
}

final class AddEmailCredsBuilder: Builder<AddEmailCredsDependency>, AddEmailCredsBuildable {

    override init(dependency: AddEmailCredsDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: AddEmailCredsListener?, mode: EditingMode) -> AddEmailCredsRouting {
        let viewController = AddEmailCredsViewController(themeStream: dependency.themeStream, keyboardObserver: dependency.keyboardEventObserver, mode: mode)
        let interactor = AddEmailCredsInteractor(presenter: viewController, backendService: dependency.backendService, keyboardObserver: dependency.keyboardEventObserver, mode: mode)
        interactor.listener = listener
        return AddEmailCredsRouter(interactor: interactor, viewController: viewController)
    }
    
    func build(withListener listener: AddEmailCredsListener?) -> AddEmailCredsRouting {
        return build(withListener: listener, mode: .create)
    }
}
