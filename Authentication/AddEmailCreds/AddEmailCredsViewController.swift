//
//  AddEmailCredsViewController.swift
//  Authentication
//

import RIBs
import RxSwift
import UIKit
import UserInterface

protocol AddEmailCredsPresentableListener: class {
    func didTapNext()
    func didEnter(email: String)
    func didEnter(password: String)
    func didEnter(confirmPassword: String)
    func isPasswordAcceptable() -> Bool
    func passwordConfrimed() -> Bool
}

final class AddEmailCredsViewController: ViewController, AddEmailCredsPresentable, AddEmailCredsViewControllable {

    weak var listener: AddEmailCredsPresentableListener?
    
    private let emailTextField: TextField
    private let passwordTextField: TextField
    private let confirmPasswordTextField: TextField
    private let purposeOfPasswordLabel: Label
    private let errorLabel: Label
    private let nextButton: Button
    private let mode: EditingMode

    init(themeStream: ThemeStreaming, keyboardObserver: KeyboardObserving? = nil, mode: EditingMode = .create) {
        self.errorLabel = Label(themeStream: themeStream, style: .smallDanger)
        self.mode = mode
        self.emailTextField = TextField(themeStream: themeStream, style: .regularPrimary)
        self.passwordTextField = TextField(themeStream: themeStream, style: .regularPrimary)
        self.purposeOfPasswordLabel = Label(themeStream: themeStream, style: .smallTertiary)
        self.nextButton = Button(themeStream: themeStream, style: .regularPillSuccess)
        self.confirmPasswordTextField = TextField(themeStream: themeStream, style: .regularPrimary)
        
        super.init(themeStream: themeStream, keyboardObserver: keyboardObserver)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }

    override func construct() {
        super.construct()
        
        switch mode {
        case .create:
            buildForCreateMode()
        case .update:
            buildForUpdateMode()
        }
        
        emailTextField.addTarget(self, action: #selector(didEnterEmail(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(didEnterPassword(_:)), for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(didEnterConfirmPassword(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(didFinishEnteringPassword), for: .editingDidEnd)
        
        emailTextField.placeholder = AddEmailCredsStrings.emailPlaceholderText
        emailTextField.keyboardType = .emailAddress
        emailTextField.enablesReturnKeyAutomatically = false
        emailTextField.autocapitalizationType = .none
        emailTextField.returnKeyType = .next
        
        passwordTextField.placeholder = AddEmailCredsStrings.passwordPlaceholderText
        passwordTextField.keyboardType = .asciiCapable
        passwordTextField.isSecureTextEntry = true
        passwordTextField.enablesReturnKeyAutomatically = false
        passwordTextField.returnKeyType = .next
        passwordTextField.delegate = self
    
        confirmPasswordTextField.placeholder = AddEmailCredsStrings.confirmPasswordPlaceholderText
        confirmPasswordTextField.keyboardType = .asciiCapable
        confirmPasswordTextField.isSecureTextEntry = true
        confirmPasswordTextField.isHidden = true
        confirmPasswordTextField.enablesReturnKeyAutomatically = false
        confirmPasswordTextField.returnKeyType = .next
        confirmPasswordTextField.delegate = self

        nextButton.setTitle(AddEmailCredsStrings.save, for: .normal)
        nextButton.addTarget(self, action: #selector(didTapNext(_:)), for: .touchUpInside)
        
        navigationItem.hidesBackButton = true
        nextButton.isEnabled = false
        
        view.backgroundColor = .green
    }
    
    override func layout() {
        super.layout()
        
        switch mode {
        case .create:
            setupConstraintsForCreate()
        case .update:
            setupConstraintsForUpdate()
        }
    }
    
    override func viewDidLayoutSubviews() {
        emailTextField.layer.cornerRadius = 0.125.dip
        passwordTextField.layer.cornerRadius = 0.125.dip
        
        emailTextField.layer.borderWidth = 0.125.dip
        passwordTextField.layer.borderWidth = 0.125.dip
        
        emailTextField.layer.borderColor = UIColor.clear.cgColor
        passwordTextField.layer.borderColor = UIColor.clear.cgColor
        
        nextButton.themeApply()
    }

    // MARK: AddNamePresentable
    var pageTitle: String = "" {
        didSet {
            title = pageTitle
        }
    }
    
    func presentError(error: AddEmailError) {

        switch error {
        case .passwordMismatch:
            passwordTextField.layer.borderColor = Color.red.cgColor
            confirmPasswordTextField.layer.borderColor = Color.red.cgColor
            errorLabel.text = "Password mismatch"
        case .passwordEmpty:
            passwordTextField.layer.borderColor = Color.red.cgColor
            errorLabel.text = "Password field cannot be empty"
        case .other(let errorString):
            errorLabel.text = errorString
        default:
            errorLabel.text = ""
        }
        
        nextButton.isEnabled = false
    }
    
    func fill(email: String) {
        emailTextField.text = email
    }

    func passwordsMatch() {
        nextButton.isEnabled = true
    }
    
    func clearError() {
        passwordTextField.layer.borderColor = UIColor.clear.cgColor
        errorLabel.text = ""
        nextButton.isEnabled = false
    }

    override func keyboardStatus(event: KeyboardEvent, height: CGFloat) {
        
        guard event == .opened || event == .closed else { return }
        
        nextButton.snp.removeConstraints()

        var computedHeight: CGFloat = 0.0
        
        if (event == .opened) {
            computedHeight = -height + gap
        } else if (event == .closed) {
            computedHeight = -gap
        }

        nextButton.snp.makeConstraints {
            $0.bottom.equalTo(view.safeAreaLayoutGuide).offset(computedHeight)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(5.dip)
        }
        
        view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    // MARK: Private methods
    private func setupConstraintsForUpdate() {
        
        emailTextField.snp.makeConstraints {
            $0.leading.top.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(3.dip)
        }
        
        passwordTextField.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.top.equalTo(emailTextField.snp.bottom).offset(topGutter * 3)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(3.dip)
        }
        
        nextButton.snp.makeConstraints {
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.bottom.equalTo(view.safeAreaLayoutGuide).offset(bottomGutter/4)
            $0.height.equalTo(5.dip)
        }
        
        errorLabel.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.top.equalTo(purposeOfPasswordLabel.snp.bottom).offset(leadingGutter/2)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(4.dip)
        }
        
        purposeOfPasswordLabel.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.top.equalTo(passwordTextField.snp.bottom).offset(topGutter)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.greaterThanOrEqualTo(3.dip)
        }
    }
    
    private func setupConstraintsForCreate() {
        
        emailTextField.snp.makeConstraints {
            $0.leading.top.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(3.dip)
        }
        
        passwordTextField.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.top.equalTo(emailTextField.snp.bottom).offset(2.5.dip)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(3.dip)
        }
        
        confirmPasswordTextField.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.top.equalTo(passwordTextField.snp.bottom).offset(2.5.dip)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(3.dip)
        }
        
        nextButton.snp.makeConstraints {
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.bottom.equalTo(view.safeAreaLayoutGuide).offset(bottomGutter/4)
            $0.height.equalTo(5.dip)
        }
        
        errorLabel.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            $0.top.equalTo(confirmPasswordTextField.snp.bottom).offset(leadingGutter/2)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.height.equalTo(4.dip)
        }
    }
    
    private func buildForCreateMode() {
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(nextButton)
        view.addSubview(confirmPasswordTextField)
        view.addSubview(errorLabel)
    }
    
    private func buildForUpdateMode() {
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(nextButton)
        view.addSubview(errorLabel)
        view.addSubview(purposeOfPasswordLabel)
        
        purposeOfPasswordLabel.numberOfLines = 10
        purposeOfPasswordLabel.text = AddEmailCredsStrings.purposeOfPassword
    }
    
    @objc private func didFinishEnteringPassword() {
        
        if listener?.isPasswordAcceptable() ?? false {
            confirmPasswordTextField.isHidden = false
        }
    }
    
    @objc private func didTapNext(_ sender: Button) {
        resignFirstResponder()
        listener?.didTapNext()
    }
    
    @objc private func didEnterEmail(_ textField: TextField) {
        
        if textField == emailTextField {
            listener?.didEnter(email: textField.text ?? "")
        }
    }
    
    @objc private func didEnterPassword(_ textField: TextField) {

        if textField == passwordTextField {
            listener?.didEnter(password: textField.text ?? "")
        }
    }
    
    @objc private func didEnterConfirmPassword(_ textField: TextField) {

        if textField == confirmPasswordTextField {
            listener?.didEnter(confirmPassword: textField.text ?? "")
        }
    }
}


extension AddEmailCredsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == passwordTextField {
            listener?.didEnter(password: textField.text ?? "")
            
            if listener?.isPasswordAcceptable() ?? false {
                textField.resignFirstResponder()
                confirmPasswordTextField.becomeFirstResponder()
                confirmPasswordTextField.isHidden = false
                return true
            }
        } else if textField == confirmPasswordTextField {
            listener?.didEnter(confirmPassword: textField.text ?? "")
            
            if listener?.passwordConfrimed() ?? false {
                textField.resignFirstResponder()
                listener?.didTapNext()
            }
        }
        
        return false
    }
}
