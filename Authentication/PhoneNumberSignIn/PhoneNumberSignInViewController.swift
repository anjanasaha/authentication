//
//  PhoneNumberSignInViewController.swift
//  Hopshop
//
//  Created by Deborshi Saha on 4/9/19.
//  Copyright © 2019 Deborshi Saha. All rights reserved.
//

import RIBs
import RxSwift
import UserInterface
import CommonLib

protocol PhoneNumberSignInPresentableListener: class {
    func didEnter(phoneNumber: String, countryCode: CountryCode)
}

final class PhoneNumberSignInViewController: ViewController, PhoneNumberSignInViewControllable {

    weak var listener: PhoneNumberSignInPresentableListener?
    
    private let placeholder = "Enter your mobile number"
    private let countryPickerView: UIImageView
    private let phoneNumberField: TextField
    private let logoView: ImageView
    private let textFormatter = USPhoneNumberTextFormatter()
    private let keyboardObserver: KeyboardObserving?
    
    override init(themeStream: ThemeStreaming, keyboardObserver: KeyboardObserving? = nil) {
        self.keyboardObserver = keyboardObserver
        self.logoView = ImageView(themeStream: themeStream)
        self.countryPickerView = ImageView(themeStream: themeStream)
        self.phoneNumberField = TextField(themeStream: themeStream, style: .regularPrimary)
        
        super.init(themeStream: themeStream, keyboardObserver: keyboardObserver)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func construct() {

        super.construct()

        logoView.image = UIImage(named: "Hopshop-Logo")

        countryPickerView.image = UIImage(named: "usa")
        countryPickerView.contentMode = .scaleAspectFill

        phoneNumberField.placeholder = placeholder
        phoneNumberField.keyboardType = .phonePad
        phoneNumberField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        phoneNumberField.defaultTextAttributes.updateValue(0.75.dip,
                                                           forKey: NSAttributedString.Key.kern)
        phoneNumberField.delegate = self

        view.addSubview(phoneNumberField)
        view.addSubview(countryPickerView)
        view.addSubview(logoView)
    }

    override func layout() {
        super.layout()

        countryPickerView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            maker.centerY.equalTo(phoneNumberField)
            maker.width.equalTo(5.dip)
            maker.height.equalTo(3.dip)
        }

        phoneNumberField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(countryPickerView.snp.trailingMargin).offset(2.dip)
            maker.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            maker.top.equalTo(logoView.snp.bottom).offset(topGutter * 0.66)
            maker.height.equalTo(8.dip)
        }

        logoView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide)
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset( leadingGutter )
            $0.trailing.lessThanOrEqualTo(view.safeAreaLayoutGuide)
        }
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {

        guard let text = textField.text else { return }

        let sanitizedText = text.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)

        if sanitizedText.count == textFormatter.maxDigits {
            listener?.didEnter(phoneNumber: sanitizedText, countryCode: .US)
        }
    }
}

extension PhoneNumberSignInViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textField = textField as? TextField, let text = textField.text else { return }
        if text.count == 0 {
            textField.style = .largePrimary
            textField.placeholder = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let textField = textField as? TextField, let text = textField.text else { return }
        if text.count == 0 {
            textField.style = .regularPrimary
            textField.placeholder = placeholder
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textField = textField as? TextField, let text = textField.text else { return true }
        let result = textFormatter.shouldChangeCharacters(replacement: string, text: text)
        textField.text = result.toText
        return result.shouldChange
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let textField = textField as? TextField, let text = textField.text else { return true }
        return text.count == 12
    }
}

extension PhoneNumberSignInViewController: PhoneNumberSignInPresentable {
    func show(alert: AlertViewModelling) {
        Alert(viewModel: alert).show(in: self)
    }
}

protocol PhoneNumberTextFormatting: class {
    var maxDigits: Int { get }
    func shouldChangeCharacters(replacement string: String, text: String) -> (shouldChange: Bool, toText: String)
}

final class USPhoneNumberTextFormatter: PhoneNumberTextFormatting {
    
    final let maxDigits: Int = 10
    
    func shouldChangeCharacters(replacement string: String, text: String) -> (shouldChange: Bool, toText: String) {

        // check string should be a digit
        
        /// 123-456-7890
        if ((text.count == 4 || text.count == 8) && isBackspace(string)), let character = text.last, character == "-" {
            let resultText = String(text.dropLast().dropLast())
            return (shouldChange: false, toText: resultText)
        } else if (text.count == 3 || text.count == 7) {
            return (shouldChange: false, toText: "\(text)-\(string)")
        }

        return (shouldChange: true, toText: text)
    }
}

enum CountryCode: String {
    case US = "+1"
}
