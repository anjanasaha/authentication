//
//  PhoneNumberSignInRouter.swift
//  Hopshop
//
//  Created by Deborshi Saha on 4/9/19.
//  Copyright © 2019 Deborshi Saha. All rights reserved.
//

import RIBs

protocol PhoneNumberSignInInteractable: Interactable, PhoneNumberVerificationListener {
    var router: PhoneNumberSignInRouting? { get set }
    var listener: PhoneNumberSignInListener? { get set }
}

protocol PhoneNumberSignInViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy.
}

final class PhoneNumberSignInRouter: ViewableRouter<PhoneNumberSignInInteractable, PhoneNumberSignInViewControllable>, PhoneNumberSignInRouting {

    private let verifyNumberBuilder: PhoneNumberVerificationBuildable
    private var verifyNumberRouter: PhoneNumberVerificationRouting?

    init(interactor: PhoneNumberSignInInteractable, viewController: PhoneNumberSignInViewControllable, verifyNumberBuilder: PhoneNumberVerificationBuildable) {
        self.verifyNumberBuilder = verifyNumberBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    func routeToVerifyNumber(phoneNumber: String) {
        let router = verifyNumberBuilder.build(withListener: interactor)
        verifyNumberRouter = router
        attachChild(router)
        if let navigationController = viewControllable.uiviewController.navigationController {
            navigationController.pushViewController(router.viewControllable.uiviewController, animated: true)
        }
    }
    
    func routeAwayFromVerifyNumber() {
        guard let router = verifyNumberRouter else { return }
        router.viewControllable.uiviewController.dismiss(animated: true, completion: nil)
        verifyNumberRouter = nil
    }
}
