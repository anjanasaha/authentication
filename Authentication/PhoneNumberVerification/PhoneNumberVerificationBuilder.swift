//
//  PhoneNumberVerificationBuilder.swift
//  Hopshop
//
//  Created by Deborshi Saha on 4/9/19.
//  Copyright © 2019 Deborshi Saha. All rights reserved.
//

import RIBs
import UserInterface

protocol PhoneNumberVerificationDependency: AddEmailCredsDependency {
    var themeStream: ThemeStreaming { get }
    var backendService: AuthenticationServicing { get }
    var keyboardEventObserver: KeyboardObserving { get }
}

final class PhoneNumberVerificationComponent: Component<PhoneNumberVerificationDependency> {
    
    var addNameBuilder: AddEmailCredsBuildable {
        return AddEmailCredsBuilder(dependency: dependency)
    }
}

// MARK: - Builder

protocol PhoneNumberVerificationBuildable: Buildable {
    func build(withListener listener: PhoneNumberVerificationListener?) -> PhoneNumberVerificationRouting
}

final class PhoneNumberVerificationBuilder: Builder<PhoneNumberVerificationDependency>, PhoneNumberVerificationBuildable {

    override init(dependency: PhoneNumberVerificationDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: PhoneNumberVerificationListener?) -> PhoneNumberVerificationRouting {
        let component = PhoneNumberVerificationComponent(dependency: dependency)
        let viewController = PhoneNumberVerificationViewController(themeStream: dependency.themeStream, keyboardObserver: dependency.keyboardEventObserver)
        let interactor = PhoneNumberVerificationInteractor(presenter: viewController, backendService: dependency.backendService, keyboardObserver: dependency.keyboardEventObserver)
        interactor.listener = listener
        return PhoneNumberVerificationRouter(interactor: interactor, viewController: viewController, addNameBuilder: component.addNameBuilder)
    }
}
