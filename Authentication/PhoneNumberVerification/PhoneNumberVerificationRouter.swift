//
//  PhoneNumberVerificationRouter.swift
//

import RIBs

protocol PhoneNumberVerificationInteractable: Interactable, AddEmailCredsListener {
    var router: PhoneNumberVerificationRouting? { get set }
    var listener: PhoneNumberVerificationListener? { get set }
}

protocol PhoneNumberVerificationViewControllable: ViewControllable { }

final class PhoneNumberVerificationRouter: ViewableRouter<PhoneNumberVerificationInteractable, PhoneNumberVerificationViewControllable>, PhoneNumberVerificationRouting {

    private let addNameBuilder: AddEmailCredsBuildable
    private var addNameRouter: AddEmailCredsRouting? = nil

    init(interactor: PhoneNumberVerificationInteractable, viewController: PhoneNumberVerificationViewControllable, addNameBuilder: AddEmailCredsBuildable) {
        self.addNameBuilder = addNameBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    func routeToAddName() {
        let router = addNameBuilder.build(withListener: interactor)
        attachChild(router)
        addNameRouter = router
        viewControllable.uiviewController.navigationController?.pushViewController(router.viewControllable.uiviewController, animated: true)
    }
    
    func routeAwayFromAddName() {
        guard let router = addNameRouter else { return }
        detachChild(router)
        addNameRouter = nil
        viewControllable.uiviewController.navigationController?.popViewController(animated: false)
    }

    func cleanUpViews() {
        guard let navigationController = viewControllable.uiviewController.navigationController else {
            return
        }
        
        navigationController.popViewController(animated: false)
    }
}
