//
//  PhoneNumberVerificationInteractor.swift
//  Hopshop
//
//  Created by Deborshi Saha on 4/9/19.
//  Copyright © 2019 Deborshi Saha. All rights reserved.
//

import RIBs
import RxSwift
import Firebase
import UserInterface

protocol PhoneNumberVerificationRouting: ViewableRouting {
    func routeToAddName()
    func routeAwayFromAddName()
    func cleanUpViews()
}

protocol PhoneNumberVerificationPresentable: Presentable, KeyboardReactable {
    var listener: PhoneNumberVerificationPresentableListener? { get set }
    
    func presentResend(title: String, enabled: Bool)
}

protocol PhoneNumberVerificationListener: class {
    func loginRequestSuccess(data: Bool)
    func loginRequestFailed(error: Error)
}

final class PhoneNumberVerificationInteractor: PresentableInteractor<PhoneNumberVerificationPresentable>, PhoneNumberVerificationInteractable, PhoneNumberVerificationPresentableListener {

    weak var router: PhoneNumberVerificationRouting?
    weak var listener: PhoneNumberVerificationListener?
    
    private var keyboardObserver: KeyboardObserving
    private let backendService: AuthenticationServicing

    init(presenter: PhoneNumberVerificationPresentable, backendService: AuthenticationServicing, keyboardObserver: KeyboardObserving) {
        self.backendService = backendService
        self.keyboardObserver = keyboardObserver
        super.init(presenter: presenter)
        presenter.listener = self
    }
    
    override func didBecomeActive() {
        super.didBecomeActive()

        backendService.authenticationStatus.subscribe(onNext: { [weak self] (status) in

            switch status {
                case .loggedIn(let user):
                    guard let user = user else {
                        return
                    }
                    if user.isNew {
                        self?.router?.routeToAddName()
                    } else {
                        self?.listener?.loginRequestSuccess(data: true)
                    }
                    
                default:
                    break
            }
        }, onError: { [weak self] (error) in
            self?.listener?.loginRequestFailed(error: error)
        }).disposeOnDeactivate(interactor: self)
        
        
        count(from: 60, to: 0, quickStart: true).subscribe(onNext: { [weak self] (secs) in
            self?.presenter.presentResend(title: PhoneNumberVerificationStrings.resendInTime("\(secs)"), enabled: false)
        }, onCompleted: { [weak self] in
            self?.presenter.presentResend(title: PhoneNumberVerificationStrings.resendInTime(), enabled: true)
        }).disposeOnDeactivate(interactor: self)
    }

    func didEnter(verificationCode: String) {
        backendService.verify(verificationCode: verificationCode)
    }
    
    func addingEmailPasswordDidSucceed() {
        router?.routeAwayFromAddName()
        listener?.loginRequestSuccess(data: true)
    }
    
    private func count(from: Int, to: Int, quickStart: Bool) -> Observable<Int> {
        
        return Observable<Int>.timer(RxTimeInterval.microseconds(0), period: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .take(from - to + 1)
            .map { from - $0 }
    }
}

