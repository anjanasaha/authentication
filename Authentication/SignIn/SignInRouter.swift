//
//  SignInRouter.swift
//  Authentication
//
//  Created by Deborshi Saha on 7/22/20.
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved.
//

import RIBs

protocol SignInInteractable: Interactable {
    var router: SignInRouting? { get set }
    var listener: SignInListener? { get set }
}

protocol SignInViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy.
}

final class SignInRouter: ViewableRouter<SignInInteractable, SignInViewControllable>, SignInRouting {

    // TODO: Constructor inject child builder protocols to allow building children.
    override init(interactor: SignInInteractable, viewController: SignInViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
