//
//  SignInInteractor.swift
//  Authentication
//
//  Created by Deborshi Saha on 7/22/20.
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved.
//

import RIBs
import RxSwift

public protocol SignInRouting: ViewableRouting {
    // TODO: Declare methods the interactor can invoke to manage sub-tree via the router.
}

public protocol SignInPresentable: Presentable {
    var listener: SignInPresentableListener? { get set }
    // TODO: Declare methods the interactor can invoke the presenter to present data.
}

public protocol SignInListener: class {
    // TODO: Declare methods the interactor can invoke to communicate with other RIBs.
}

final class SignInInteractor: PresentableInteractor<SignInPresentable>, SignInInteractable, SignInPresentableListener {

    weak var router: SignInRouting?
    weak var listener: SignInListener?

    // TODO: Add additional dependencies to constructor. Do not perform any logic
    // in constructor.
    override init(presenter: SignInPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        // TODO: Implement business logic here.
    }

    override func willResignActive() {
        super.willResignActive()
        // TODO: Pause any business logic.
    }
}
