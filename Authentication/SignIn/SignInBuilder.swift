//
//  SignInBuilder.swift
//  Authentication
//
//

import RIBs

public protocol SignInDependency: Dependency {
    // TODO: Declare the set of dependencies required by this RIB, but cannot be
    // created by this RIB.
}

final class SignInComponent: Component<SignInDependency> {

    // TODO: Declare 'fileprivate' dependencies that are only used by this RIB.
}

// MARK: - Builder

public protocol SignInBuildable: Buildable {
    func build(withListener listener: SignInListener) -> SignInRouting
}

public final class SignInBuilder: Builder<SignInDependency>, SignInBuildable {

    override init(dependency: SignInDependency) {
        super.init(dependency: dependency)
    }

    public func build(withListener listener: SignInListener) -> SignInRouting {
        _ = SignInComponent(dependency: dependency)
        let viewController = SignInViewController()
        let interactor = SignInInteractor(presenter: viewController)
        interactor.listener = listener
        return SignInRouter(interactor: interactor, viewController: viewController)
    }
}
