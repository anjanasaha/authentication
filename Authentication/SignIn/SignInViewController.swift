//
//  SignInViewController.swift
//  Authentication
//
//  Created by Deborshi Saha on 7/22/20.
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

public protocol SignInPresentableListener: class {
    // TODO: Declare properties and methods that the view controller can invoke to perform
    // business logic, such as signIn(). This protocol is implemented by the corresponding
    // interactor class.
}

final class SignInViewController: UIViewController, SignInPresentable, SignInViewControllable {

    weak var listener: SignInPresentableListener?
}
