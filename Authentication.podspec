Pod::Spec.new do |s|
    s.platform = :ios
    s.ios.deployment_target = '13.0'
    s.name = "Authentication"
    s.summary = "Authentication lets a user sign in."
    s.requires_arc = true
    s.version = "0.1.0"
    s.homepage         = 'https://anjanasaha@bitbucket.org/anjanasaha/authentication.git'
    s.license          = { :type => 'MIT', :file => 'LICENSE.txt' }
    s.author           = { 'anjanasaha' => 'anjanasaha26@gmail.com' }
    s.source           = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/authentication.git', :tag => 'v' + s.version.to_s }

    # Public
    s.dependency 'Firebase'
    s.dependency 'Firebase/Core'
    s.dependency 'Firebase/Auth'

    # Private
    s.dependency 'RIBs', '0.1.0'
    s.dependency 'CommonLib', '0.1.0'
    s.dependency 'UserInterface', '0.1.0'
    
    s.frameworks = 'MapKit', 'Foundation', 'SystemConfiguration', 'CoreText', 'QuartzCore', 'Security', 'UIKit', 'Foundation', 'CoreGraphics','CoreTelephony', 'FirebaseCore', 'FirebaseRemoteConfig', 'FirebaseInstanceID', 'FirebaseAnalytics', 'FirebaseABTesting', 'FirebaseCoreDiagnostics', 'FirebaseNanoPB'
    s.libraries = 'c++', 'sqlite3', 'z'

    s.source_files = "Authentication/**/*.{swift}"
    s.swift_version = "5.0"

    s.pod_target_xcconfig = {
        'FRAMEWORK_SEARCH_PATHS' => '$(inherited) $(PODS_ROOT)/Firebase $(PODS_ROOT)/FirebaseCore/Frameworks $(PODS_ROOT)/FirebaseRemoteConfig/Frameworks $(PODS_ROOT)/FirebaseInstanceID/Frameworks $(PODS_ROOT)/FirebaseAnalytics/Frameworks $(PODS_ROOT)/FirebaseABTesting/Frameworks'
    }
    
    s.pod_target_xcconfig = {
        'OTHER_LDFLAGS' => '$(inherited) -ObjC'
    }
end
